ARG BASE_IMAGE=quay.io/keycloak/keycloak:23.0
FROM $BASE_IMAGE as builder

ENV KC_METRICS_ENABLED=true
ENV KC_DB=postgres
WORKDIR /opt/keycloak
RUN /opt/keycloak/bin/kc.sh build

FROM $BASE_IMAGE
COPY --from=builder /opt/keycloak/ /opt/keycloak/
WORKDIR /opt/keycloak
